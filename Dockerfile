
FROM gcr.io/kubeflow-images-public/tensorflow-2.0.0a0-notebook-gpu:v-base-08f3cbc-1166369568336121856
LABEL maintainer="bjorn.annergren@arbetsformedlingen.se"

# Ensure important locations are in path
ENV PATH $CONDA_DIR/bin:$PATH
ENV PATH /home/jovyan/.local/bin:$PATH

# Set shell to bash
SHELL ["/bin/bash", "-c"]

# Due to base-image user is set to jovyan thus have to switch to root for installing global dependencies
USER root
RUN apt-get update
RUN apt-get -y install autoconf automake libtool curl make g++ unzip libsnappy-dev

# --------------------------------------------------
# Install Implicit rekommendation framework
# --------------------------------------------------

# Have to run before nvcc installation due to ld linking issues
USER jovyan
RUN pip install implicit
# Hotfix until official image is updated
RUN pip install --upgrade tensorflow-gpu==2.0.0-rc1

# --------------------------------------------------
# Install nvcc. Needed for Apex
# --------------------------------------------------

USER root
# Download and Install nvcc by installing CUDA Toolkit Conda CUDA Toolkit does not install it
RUN wget https://developer.nvidia.com/compute/cuda/10.0/Prod/local_installers/cuda_10.0.130_410.48_linux
RUN mv cuda_10.0.130_410.48_linux cuda_10.0.130_410.48_linux.run && \
    sh cuda_10.0.130_410.48_linux.run --silent --toolkit --override
ENV LD_LIBRARY_PATH /usr/local/cuda-10.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
RUN cp /usr/local/cuda-10.0/lib64/libcudart.so.10.0 /usr/local/cuda-10.0/lib64/libcudart.so
# Clean files
RUN rm cuda_10.0.130_410.48_linux.run 


# --------------------------------------------------
# Install base requirements
# --------------------------------------------------

# Have to swap back to jovyan to have correct permissions for /home/jovyan
USER jovyan
RUN pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN rm requirements.txt

# --------------------------------------------------
# Install Apex
# --------------------------------------------------

RUN git clone https://github.com/NVIDIA/apex && \
    cd apex && \
    pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./

# --------------------------------------------------
# Install Tensorflow Transform, TFX and Protoc for TFX
# --------------------------------------------------

USER root
# Install Protoc 3.6.1 Binary Blob
RUN wget https://github.com/protocolbuffers/protobuf/releases/download/v3.6.1/protoc-3.6.1-linux-x86_64.zip && \
    unzip protoc-3.6.1-linux-x86_64.zip -d protoc3 && \
    mv protoc3/bin/* /usr/local/bin/ && \
    mv protoc3/include/* /usr/local/include/ &&\
    chown jovyan /usr/local/bin/protoc && \
    chown -R jovyan /usr/local/include/google && \
    ldconfig
# Clean files
RUN rm protoc-3.6.1-linux-x86_64.zip && rm -r  protoc3

USER jovyan

# Install Tensorflow Transform 
RUN git clone https://github.com/tensorflow/transform.git && \
    cd transform && \
    pip install --user .

# Install TFX
RUN git clone https://github.com/tensorflow/tfx.git && \
    cd tfx && \
    pip install . 

# --------------------------------------------------
# Install Jupyterlab Tensorboard. 
# --------------------------------------------------

RUN conda install nodejs
RUN jupyter labextension install jupyterlab_tensorboard
RUN jupyter tensorboard enable --user


# --------------------------------------------------
# Install support for Kubeflow pipelines
# --------------------------------------------------

RUN pip install --trusted-host storage.googleapis.com  https://storage.googleapis.com/ml-pipeline/release/0.1.4/kfp.tar.gz --upgrade

# --------------------------------------------------
# For the rest of the dependencies have to be root again
# --------------------------------------------------

USER root

# --------------------------------------------------
# Minio command line tool
# --------------------------------------------------

RUN wget https://dl.minio.io/client/mc/release/linux-amd64/mc
RUN chmod +x mc
RUN mv mc /usr/bin

# --------------------------------------------------
# Datalake support
# --------------------------------------------------

RUN apt-get install -y krb5-user
RUN apt-get install -y libsasl2-dev libsasl2-2 libsasl2-modules-gssapi-mit

COPY datalake-environment.yml datalake-environment.yml
RUN conda env update -f datalake-environment.yml

# --------------------------------------------------
# Install kafka
# --------------------------------------------------

RUN apt-get install -y kafkacat

# --------------------------------------------------
# Openshift Source-To-Image command line tool
# --------------------------------------------------

RUN wget https://github.com/openshift/source-to-image/releases/download/v1.1.13/source-to-image-v1.1.13-b54d75d3-linux-amd64.tar.gz
RUN tar -xvf source-to-image-v1.1.13-b54d75d3-linux-amd64.tar.gz
RUN mv s2i /usr/bin
# Clean files
RUN rm source-to-image-v1.1.13-b54d75d3-linux-amd64.tar.gz

# --------------------------------------------------
# Oracle client
# --------------------------------------------------

RUN apt-get install libaio1
RUN mkdir /opt/oracle
WORKDIR /opt/oracle
COPY instantclient_19_3 /opt/oracle/instantclient_19_3
RUN sh -c "echo /opt/oracle/instantclient_18_3 > /etc/ld.so.conf.d/oracle-instantclient.conf"
RUN ldconfig
WORKDIR /home/jovyan

# --------------------------------------------------
# Kerberos support
# --------------------------------------------------

RUN adduser shaed --gecos --disabled-password && \
    adduser jupyter --gecos --disabled-password

# Configure kerberos
COPY krb5.conf.jovyan /etc/krb5.conf
COPY keytabs /home/jovyan/keytabs

RUN sudo -H -u jovyan bash -c 'touch /tmp/krb5cc_jovyan'
RUN sudo -H -u jovyan bash -c 'chmod 777 /tmp/krb5cc_jovyan'
RUN sudo -H -u jovyan bash -c 'kinit  jupyterhub/silver.ws.ams.se@WP.AMS.SE -k -t  /home/jovyan/keytabs/WS_Silver_ny.keytab' \ 
        || echo "You are not allowed to get a ticket on this server"
COPY code /home/jovyan/keytabs/code
# Setup for the sudospawner case
RUN mkdir -p /opt/jupyterhub && \
    chown jupyter:jupyter /opt/jupyterhub && \
    echo 'jupyter ALL=(ALL:ALL) NOPASSWD:/opt/conda/bin/sudospawner' >> /etc/sudoers

# --------------------------------------------------
# Finalize
# --------------------------------------------------

# Want a functional Bash 
RUN sudo rm /bin/sh
RUN sudo ln -s /bin/bash /bin/sh

RUN conda clean --yes --all
WORKDIR /home/jovyan
CMD ["sh","-c", "jupyter notebook --notebook-dir=/home/jovyan --ip=0.0.0.0 --no-browser --allow-root --port=8888 --NotebookApp.token='' --NotebookApp.password='' --NotebookApp.allow_origin='*' --NotebookApp.base_url=${NB_PREFIX}"]
