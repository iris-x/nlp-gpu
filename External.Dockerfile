# @ the time latest tensorflow 2.0 GPU image
FROM gcr.io/kubeflow-images-public/tensorflow-2.0.0a0-notebook-gpu:v-base-08f3cbc-1166369568336121856
LABEL maintainer="bjorn.annergren@arbetsformedlingen.se"

# Ensure important locations are in path
ENV PATH $CONDA_DIR/bin:$PATH
ENV PATH /home/jovyan/.local/bin:$PATH

# Set shell to bash
SHELL ["/bin/bash", "-c"]

# Due to base-image user is set to jovyan thus have to switch to root for installing global dependencies
USER root
RUN apt-get update
RUN apt-get install -y htop 

# --------------------------------------------------
# Install base requirements
# --------------------------------------------------

# Have to swap back to jovyan to have correct permissions for /home/jovyan
USER jovyan
# Hotfix until official image is updated
RUN pip install --upgrade tensorflow-gpu==2.0.0-rc1
RUN pip install --upgrade pip
RUN pip install torch==1.2.0
RUN pip install pytorch-transformers==1.2.0
RUN pip install tensorboardX==1.8
RUN pip install seqeval==0.0.12
RUN pip install xgboost==0.90
RUN pip install pytest==5.1.2
RUN pip install scikit-learn==0.21.3
RUN pip install pytorch_pretrained_bert==0.6.2
RUN pip install nltk==3.4.5

# --------------------------------------------------
# Install Apex
# --------------------------------------------------

USER root
# Download and Install nvcc by installing CUDA Toolkit Conda CUDA Toolkit does not install it
RUN wget https://developer.nvidia.com/compute/cuda/10.0/Prod/local_installers/cuda_10.0.130_410.48_linux
RUN mv cuda_10.0.130_410.48_linux cuda_10.0.130_410.48_linux.run && \
    sh cuda_10.0.130_410.48_linux.run --silent --toolkit --override
ENV LD_LIBRARY_PATH /usr/local/cuda-10.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
RUN cp /usr/local/cuda-10.0/lib64/libcudart.so.10.0 /usr/local/cuda-10.0/lib64/libcudart.so

# Clean files
RUN rm cuda_10.0.130_410.48_linux.run 

# Install Apex
RUN git clone https://github.com/NVIDIA/apex && \
    cd apex && \
    pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./ 

# --------------------------------------------------
# Finalize
# --------------------------------------------------

# Want a functional Bash 
RUN sudo rm /bin/sh
RUN sudo ln -s /bin/bash /bin/sh

USER jovyan
RUN conda clean --yes --all
WORKDIR /home/jovyan
CMD ["sh","-c", "jupyter notebook --notebook-dir=/home/jovyan --ip=0.0.0.0 --no-browser --allow-root --port=8888 --NotebookApp.token='' --NotebookApp.password='' --NotebookApp.allow_origin='*' --NotebookApp.base_url=${NB_PREFIX}"]
