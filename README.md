
# NLP GPU Environment
Base image for Tensorflow Transform, TFX, Torch, Transformers targeting a GPU environment for Kubeflow 0.6.
Contains miscellanous ML packages as well. Update requirements.txt if dependencies are missing.

## External.Dockerfile
Environment that is not integrated with Data Lake etc. To be used with external server. Once capacity increased for AI-platform will be removed.

Should be able to build anywhere.

## Building 
Image must be built on correct AI-platform node as certain files regarding Data Lake support can not be committed to repo.

### Building instructions
TODO! See master branch for an indication of build procedure.

### Building and running on external server
```
docker build -f External.Dockerfile -t nlp-gpu .
docker run -p 9090:8888 --name nlp-gpu -v /home/magnus/dl-containers/:/home/jovyan nlp-gpu
```

## TODO
* Test in proper Kubeflow environment
    * GPU support
* Investigate proper build procedure